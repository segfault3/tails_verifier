#!/usr/bin/env python3

import sys
import os
import mount
import re

usage = """usage: {0} <partition>
Example: {0} /dev/sdc1""".format(sys.argv[0])


def get_versions(partition_path):
    if not os.geteuid() == 0:
        sys.exit('This script must be run as root')
    with mount.Mount(partition_path) as partition:
        return get_versions_from_squashfs_filenames(partition)


def get_versions_from_squashfs_filenames(partition):
    versions = list()
    live_dir = os.path.join(partition.mount_point, "live")
    live_dir_files = os.listdir(live_dir)
    for file_name in live_dir_files:
        match = re.match("(\d.*)\.squashfs", file_name)
        if match:
            versions.append(match.group(1))

    # Sort the versions as if chr(0) and '.' was appended to each of them
    # This way e.g. 2.0~rc1 comes before 2.0, but 2.0.1 still comes after 2.0
    # TODO: This is ugly and it's not immediately clear why this works.
    versions.sort(key=lambda x: x.replace('~', chr(0)) + '.')
    return versions


def parse_args():
    if len(sys.argv) != 2:
        sys.exit(usage)
    return sys.argv[1]


def main():
    partition_path = parse_args()
    print(get_versions(partition_path))


if __name__ == "__main__":
    main()
